package com.digicommerce.demo.controller;

import com.digicommerce.demo.consumer.HanaVehicleService;
import com.digicommerce.demo.dto.VehicleData;
import com.sap.cloud.sdk.odatav2.connectivity.ODataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/vehicles")
public class VehicleController
{

	@Autowired
	private HanaVehicleService vehicleService;

	@GetMapping("/{vehicleId}")
	@ResponseBody
	public VehicleData getVehicle(@PathVariable("vehicleId") final String vehicleId) throws ODataException
	{
 		final VehicleData vehicle = vehicleService.getVehicle(vehicleId);

		return vehicle;
	}
}
