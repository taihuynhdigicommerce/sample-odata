package com.digicommerce.demo.consumer;

import com.digicommerce.demo.dto.VehicleData;
import com.sap.cloud.sdk.odatav2.connectivity.ODataException;
import com.sap.cloud.sdk.odatav2.connectivity.ODataQueryBuilder;
import com.sap.cloud.sdk.odatav2.connectivity.ODataQueryResult;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Base64;


@Component
public class HanaVehicleService
{

	private static final String PREFIX_AUTHORIZATION = "Basic ";
	private static final String AUTH_HEADER_KEY = "Authorization";

	@Value("${sap.hana.saaq.base.url}")
	private String baseUrl;

	@Value("${sap.hana.saaq.username}")
	private String user;

	@Value("${sap.hana.saaq.password}")
	private String password;

	@Value("${sap.hana.saaq.service.path}")
	private String servicePath;

	private String entityName = "xSAAQxZ_I_ZEQUI_PERMIS";

	public VehicleData getVehicle(final String vehicleId) throws ODataException{

				HttpClient httpClient = HttpClientBuilder.create().build();

			 final ODataQueryResult result = this.buildQueryBuilder(this.entityName + "('" + vehicleId + "')")
					.expand("to_PermisClass", "to_PermisCondition","to_PermisMention","to_OthCharacteristics","to_PermisStatus")
					.build().execute(httpClient);

			 return result.as(VehicleData.class);
	}

	private ODataQueryBuilder buildQueryBuilder(final String entityName) {
		return ODataQueryBuilder.withEntity(this.baseUrl, entityName)
				.withHeader(this.AUTH_HEADER_KEY, this.buildBasicAuth(), true)
				.param("sap-language", "E")
				.param("format", "json");
	}

	private String buildBasicAuth() {
		final String authorizationKey = this.user + ":" + this.password;
		return this.PREFIX_AUTHORIZATION + Base64.getEncoder().encodeToString(authorizationKey.getBytes());
	}
}
