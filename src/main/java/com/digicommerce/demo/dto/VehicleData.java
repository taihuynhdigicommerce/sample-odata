package com.digicommerce.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class VehicleData implements Serializable
{

	@JsonProperty("Equipment")
	@SerializedName("Equipment")
	private String licenceNumber;

	@JsonProperty("InventoryNo")
	@SerializedName("InventoryNo")
	private String numReference;

	public String getLicenceNumber()
	{
		return licenceNumber;
	}

	public void setLicenceNumber(String licenceNumber)
	{
		this.licenceNumber = licenceNumber;
	}

	public String getNumReference()
	{
		return numReference;
	}

	public void setNumReference(String numReference)
	{
		this.numReference = numReference;
	}
}
